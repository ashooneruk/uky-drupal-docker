set -e

echo >&2 "Starting UKY drupal init script"
if [[ -n "${DEBUG}" ]]; then
    set -x
fi

echo >&2 "Checking for ${APP_NAME} in ${APP_ROOT}..."
if ! [ -e "${DRUPAL_ROOT}/index.php" ]; then
    echo >&2 "${APP_NAME} not found in ${APP_ROOT} - copying now..."
    rsync -rlt "/usr/src/drupal/" "${APP_ROOT}/"
    echo >&2 "Complete! ${APP_NAME} has been successfully copied to ${APP_ROOT}"
  else
    echo >&2 "${APP_NAME} found! Skipping copy to ${APP_ROOT}..."
fi
echo >&2 "Finishing UKY drupal init script"
