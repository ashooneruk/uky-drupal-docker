FROM wodby/drupal-php:5.6

RUN set -xe && su-exec root mkdir /usr/src/drupal/

COPY composer.json /usr/src/drupal/

RUN set -xe && \
    chown www-data:www-data /usr/src/drupal && \
    su-exec www-data composer install -d /usr/src/drupal/ -n -v

COPY init /docker-entrypoint-init.d/
